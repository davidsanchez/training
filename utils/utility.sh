#!/usr/bin/env bash

echo "Starting search ...."
echo -ne '#                     (1%)\r'
sleep 1

echo "Creating tmp file in /tmp/..."
BASE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "/tmp" || return "$1"
OUTPUT="/tmp/"$(mktemp XXXXXX)
cd || return "$1"

function pattern_search () {
    LANG_EXT="$1"
    LANG="$2"
    printf "=== Directorys without %s files located === \n" "$LANG" >> "$OUTPUT"
    printf "================================================ \n" >> "$OUTPUT"
    PATTERN="ls -l '{}' | egrep -i -q '^.($LANG_EXT)$'"
    PATH_CHALL=$BASE_DIR"/challenges/codeabbey/"
    find "$PATH_CHALL" -mindepth 1 -maxdepth 2 -type d '!' -exec sh -c "$PATTERN" ';' -print >> "$OUTPUT"
           printf "=== End of search === \n\n " >> "$OUTPUT"
}
echo -ne '#####                     (33%)\r'
sleep 1

printf "==== Challenges without solution but that ONLY contains OTHERS.lst LINK.lst files ==== \n" >> "$OUTPUT"
PATH_CHALLS=$BASE_DIR"/challenges/"
LOCATED_DIRS=$(find "$PATH_CHALLS" -maxdepth 2 -type d -exec test -e '{}'/OTHERS.lst -a -e '{}'/LINK.lst  \; -print)
ARRAY_DIRS=($LOCATED_DIRS)

for dir in "${ARRAY_DIRS[@]}"
do
    PATTERN='ls -1 "{}"| grep -E -iq "*\.feature$"'
    CHECK=$(find "$dir" -type d '!' -exec sh -c "$PATTERN" ';' -print)
    if [ "$CHECK" != "" ];
    then
        FILE_COUNT=$(find "$CHECK" -maxdepth 1 -type f 2>/dev/null | wc -l)
        if [ "$FILE_COUNT" -eq "2" ];
        then
            echo "$CHECK" >> "$OUTPUT"
        fi
    fi
done
printf "==== End of search === \n\n" >> "$OUTPUT"

echo -ne '#############             (66%)\r'
sleep 1

# This script does a very simple check for directorys missing certain pl file
PLANG=(Bash C C\# Go Haskell Java Javascript Pascal Perl Python)

for item in "${PLANG[@]}";
do
    case $item in
        "Bash")
            pattern_search "sh" "$item"
            ;;
        "C")
            pattern_search "c" "$item"
            ;;
        "C#")
            pattern_search "cs" "$item"
            ;;
        "Go")
            pattern_search "go" "$item"
            ;;
        "Haskell")
            pattern_search "hs" "$item"
            ;;
        "Java")
            pattern_search "java" "$item"
            ;;
        "Javascript")
            pattern_search "js" "$item"
            ;;
        "Pascal")
            pattern_search "pas" "$item"
            ;;
        "Perl")
            pattern_search "pl" "$item"
            ;;
        "Python")
            pattern_search "py" "$item"
            ;;
    esac
done

echo -ne '#######################   (100%)\r'
echo -ne '\n'

echo "Check the file $OUTPUT"
