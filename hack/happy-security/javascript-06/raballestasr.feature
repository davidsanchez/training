# language: en

Feature: Solve the challenge foreach
  From the happy-security.de website
  From the JavaScript category
  With my username raballestas

  Background:
    Given an input

  Scenario: Successful solution
    When I look into the source code
    Then I see the submit calls a javascript function get_passwd()
    When I look into the code for the function get_passwd()
    Then I see the function encodes the given value
    And I see the encoded password must be "Sdxjt"
    Then I decode "Sdxjt" by hand
    Then I write that value in the input
    And I solve the challenge
