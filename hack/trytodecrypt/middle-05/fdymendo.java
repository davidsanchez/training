package desencriptar;

import java.util.ArrayList;
import java.util.HashMap;

public class Desencriptar {
    public static ArrayList<String> almaTextSin_Encr = new ArrayList<>();
    public static ArrayList<String> almaTextEncr = new ArrayList<>();
    public static HashMap<String,String> grupo1 = new HashMap<>();
    public static HashMap<String,String> grupo2 = new HashMap<>();
    public static HashMap<String,String> grupo3 = new HashMap<>();
    public static HashMap<String,String> grupo4 = new HashMap<>();
    public static HashMap<String,String> grupo5 = new HashMap<>();
    public static HashMap<String,String> grupo6 = new HashMap<>();

    public static void main(String[] args) {
        rellenar();
        String textDese="";
        String textParaDeseStr = "3785824AD56B2531A7150DF44C21434A61E6"
                + "3F040A42F2012BC2F43F0AD535D24D46013213866D7E0";
        for(int i =0;i<almaTextEncr.size();i++){
                cargar2(almaTextSin_Encr.get(i),almaTextEncr.get(i));
        }
        for(int i=3,j=0,k=1;i<=textParaDeseStr.length();i+=3,k++){
        if(k==7)
            k=1;
        if(k==1)
            textDese += grupo1.get(textParaDeseStr.substring(j, i));
        if(k==2)
            textDese += grupo2.get(textParaDeseStr.substring(j, i));
        if(k==3)
            textDese += grupo3.get(textParaDeseStr.substring(j, i));
        if(k==4)
            textDese += grupo4.get(textParaDeseStr.substring(j, i));
        if(k==5)
            textDese += grupo5.get(textParaDeseStr.substring(j, i));
        if(k==6)
            textDese += grupo6.get(textParaDeseStr.substring(j, i));
        j=i;
        }
        System.out.println(textDese);
    }

     private static void cargar2(String textSin_Encr,String textEncr){
        grupo1.put(""+textEncr.substring(0, 3), ""+textSin_Encr.charAt(0));
        grupo2.put(""+textEncr.substring(3, 6), ""+textSin_Encr.charAt(1));
        grupo3.put(""+textEncr.substring(6, 9), ""+textSin_Encr.charAt(2));
        grupo4.put(""+textEncr.substring(9, 12), ""+textSin_Encr.charAt(3));
        grupo5.put(""+textEncr.substring(12, 15), ""+textSin_Encr.charAt(4));
        grupo6.put(""+textEncr.substring(15, 18), ""+textSin_Encr.charAt(5));
    }

     private static void rellenar(){
        cargar("aaaaaa", "1202F03090F725310E");
        cargar("bbbbbb", "12C31F31E10A276117");
        cargar("cccccc", "13834E33311D299120");
        cargar("dddddd", "14437D3481302BC129");
        cargar("eeeeee", "1503AC35D1432DF132");
        cargar("ffffff", "15C3DB37215630213B");
        cargar("gggggg", "16840A387169325144");
        cargar("hhhhhh", "17443939C17C34814D");
        cargar("iiiiii", "1804683B118F36B156");
        cargar("jjjjjj", "18C4973C61A238E15F");
        cargar("kkkkkk", "1984C63DB1B53B1168");
        cargar("llllll", "1A44F53F01C83D4171");
        cargar("mmmmmm", "1B05244051DB3F717A");
        cargar("nnnnnn", "1BC55341A1EE41A183");
        cargar("oooooo", "1C858242F20143D18C");
        cargar("pppppp", "1D45B1444214460195");
        cargar("qqqqqq", "1E05E045922748319E");
        cargar("rrrrrr", "1EC60F46E23A4A61A7");
        cargar("ssssss", "1F863E48324D4C91B0");
        cargar("tttttt", "20466D4982604EC1B9");
        cargar("uuuuuu", "21069C4AD27350F1C2");
        cargar("vvvvvv", "21C6CB4C22865321CB");
        cargar("wwwwww", "2286FA4D72995551D4");
        cargar("xxxxxx", "2347294EC2AC5781DD");
        cargar("yyyyyy", "2407585012BF59B1E6");
        cargar("zzzzzz", "24C7875162D25BE1EF");
        cargar("AAAAAA", "2587B652B2E55E11F8");
        cargar("BBBBBB", "2647E55402F8604201");
        cargar("CCCCCC", "27081455530B62720A");
        cargar("DDDDDD", "27C84356A31E64A213");
        cargar("EEEEEE", "28887257F33166D21C");
        cargar("FFFFFF", "2948A1594344690225");
        cargar("GGGGGG", "2A08D05A93576B322E");
        cargar("HHHHHH", "2AC8FF5BE36A6D6237");
        cargar("IIIIII", "2B892E5D337D6F9240");
        cargar("JJJJJJ", "2C495D5E839071C249");
        cargar("KKKKKK", "2D098C5FD3A373F252");
        cargar("LLLLLL", "2DC9BB6123B676225B");
        cargar("MMMMMM", "2E89EA6273C9785264");
        cargar("NNNNNN", "2F4A1963C3DC7A826D");
        cargar("OOOOOO", "300A486513EF7CB276");
        cargar("PPPPPP", "30CA776664027EE27F");
        cargar("QQQQQQ", "318AA667B415811288");
        cargar("RRRRRR", "324AD5690428834291");
        cargar("SSSSSS", "330B046A543B85729A");
        cargar("TTTTTT", "33CB336BA44E87A2A3");
        cargar("UUUUUU", "348B626CF46189D2AC");
        cargar("VVVVVV", "354B916E44748C02B5");
        cargar("WWWWWW", "360BC06F94878E32BE");
        cargar("XXXXXX", "36CBEF70E49A9062C7");
        cargar("YYYYYY", "378C1E7234AD9292D0");
        cargar("ZZZZZZ", "384C4D7384C094C2D9");
        cargar("000000", "0A811A2370390F50B4");
        cargar("111111", "0B414924C04C1180BD");
        cargar("222222", "0C017826105F13B0C6");
        cargar("333333", "0CC1A727607215E0CF");
        cargar("444444", "0D81D628B0851810D8");
        cargar("555555", "0E42052A00981A40E1");
        cargar("666666", "0F02342B50AB1C70EA");
        cargar("777777", "0FC2632CA0BE1EA0F3");
        cargar("888888", "1082922DF0D120D0FC");
        cargar("999999", "1142C12F40E4230105");
        cargar("______", "39CCAB7624E69922EB");
        cargar("......", "3A8CDA7774F99B52F4");
        cargar(",,,,,,", "3B4D0978C50C9D82FD");
        cargar(";;;;;;", "3C0D387A151F9FB306");
        cargar("::::::", "3CCD677B6532A1E30F");
        cargar("??????", "3D8D967CB545A41318");
        cargar("!!!!!!", "3E4DC57E0558A64321");
        cargar("      ", "3F0DF47F556BA8732A");
    }

     private static void cargar(String tex1,String tex2){
        almaTextSin_Encr.add(tex1);
        almaTextEncr.add(tex2);
    }
}
