#lenguage: en

Feature: Solve PHP 0818 challenge
  From the Wechall site
  Of the category Exploit and PHP
  As the registered user disassembly

  Background:
    Given a form to send the magic number
    And the challenge source code

  Scenario Outline: Enter the magic number

  Scenario: Try to enter the number directly
    Given the input text box and the submit button
    And the magic number that is in the source code
    """
    37  # Allow the magic number ...
    38  return $number == "3735929054";
    """
    Then I enter the number as it is
    But show me that: Your magic number is obviously wrong!

  Scenario: Attempt to bypass the constrains that disallow digits
    Given a condition that don't allow digits betwen one and nine
    """
    23  $one = ord('1');
    24  $nine = ord('9');
    25  # Check all the input characters!
    26  for ($i = 0; $i < strlen($number); $i++)
    27  {
    28    # Disallow all the digits!
    29    $digit = ord($number{$i});
    30    if ( ($digit >= $one) && ($digit <= $nine) )
    31    {
    32      # Aha, digit not allowed!
    33      return false;
    34    }
    35  }
    """
    Given I enter diferent combinations of 0 and e, eg. 0.0 and 0e0
    And try to fool the comparison operator with manipulation of types
    But none succeeded

  Scenario: Looking for more information about the manipulation of types
    Given PHP is a loosely typed lenguage
    And the numerical string comparison is done in a particular way
    Given I try different methods to make true the equal comparison
    And realize that the digit 0 is allowed
    Then I know the number can be enter in HEX base

  Scenario: Bypassing the if condition
    Given a web page to test PHP scripts, I enter:
    """
    1   <?php
    2   echo var_dump("0xF" == "15");
    """
    But it give me
    """
    bool(false)
    """
    Then I notice that the PHP version is 7.0
    And HEX literals are not recognised in strings anymore
    Then I change the version to 5.6 and this time return me true
    Given a tool online to convert decimal numbers to HEX
    And the magic number 3735929054, the tool shows me:
    """
    0xdeadc0de
    """
    Then I enter that string and solve the challenge
