# lenguage: en

Feature: Solve Crappyshare challenge
  From the WeChall site
  Of the category Exploit and PHP
  As the registered user disassembly

  Background:
    Given a web page to upload files from the local machine or by URL
    And the source code with the XHTML/CSS and PHP in it

  Scenario: Look for vulnerabilities in the code
    Given the upload script seems to be clean
    But the first line shows a hint
    """
    1  <?php require_once 'solution.php'; # <-- Look closer to this file! ?>
    """
    Then I know that the solution file is in the local directory

  Scenario: Test some logic vulnerabilities
    When I upload a file with the same name as the solution
    Then the web page doesn't show me any content since it's not stored
    And only displays the file contents
    When I upload the solution file by his url (or at least I think it is)
    """
    https://www.wechall.net/challenge/crappyshare/solution.php
    """
    But again it doesn't show me nothing

  Scenario: Search for types of vulnerabilities
    Given the fact that maybe it can be a LFI vulnerability
    But I don't find anything sligthly similar
    Then I look for other attacks to read internal files
    And I found a type called SSRF (Server Side Request Forgery)

  Scenario: Expose solution.php file via SSRF
    Given the text box to upload files by URL
    And that the file is in the current directory
    When I use the URI:
    """
    file:///solution.php
    """
    Then the web page display the keyword to solve the challenge
    """
    <?php
    #########################
    ### ICaNSeEcLeArLyNoW ###
    #########################
    ?>
    """
