# ruby-lint kergrau.rb
answer = ''
letter = ''

File.open('DATA.lst', 'r') do |file|
  while line = file.gets

    baconian = line.gsub(/\W/, '').strip

    baconian.chars.each do |x|
      letter = /[a-z]/.match(x)? letter << 'a' : letter << 'b'

      if letter.length == 5

        case letter
          when 'aaaaa'
            answer << 'A'
          when 'aaaab'
            answer << 'B'
          when 'aaaba'
            answer << 'C'
          when 'aaabb'
            answer << 'D'
          when 'aabaa'
            answer << 'E'
          when 'aabab'
            answer << 'F'
          when 'aabba'
            answer << 'G'
          when 'aabbb'
            answer << 'H'
          when 'abaaa'
            answer << 'I'
          when 'abaab'
            answer << 'J'
          when 'ababa'
            answer << 'K'
          when 'ababb'
            answer << 'L'
          when 'abbaa'
            answer << 'M'
          when 'abbab'
            answer << 'N'
          when 'abbba'
            answer << 'O'
          when 'abbbb'
            answer << 'P'
          when 'baaaa'
            answer << 'Q'
          when 'baaab'
            answer << 'R'
          when 'baaba'
            answer << 'S'
          when 'baabb'
            answer << 'T'
          when 'babaa'
            answer << 'U'
          when 'babab'
            answer << 'V'
          when 'babba'
            answer << 'W'
          when 'babbb'
            answer << 'X'
          when 'bbaaa'
            answer << 'Y'
          when 'bbaab'
            answer << 'Z'
        end
        letter = ''
      end
    end
  end
end
puts answer

# ruby kergrau.rb
# VERYXWELLXDONEXFELLOWXHACKERXTHEXSECRETXKEYWORDXISXEEPHCMFMAMHGXXKVFKSUJO
# UWKWWURNWVFNFWJKSVEWVLKXLKJNJVMTMTEVLKUVJFKNKZEUVUVSKKSZKTNKWVKVSUSOEVWVJ
# KKZKVKWULKZKOVLKNKLUVNKNFSUSVVKKZKJKLKJKVKJKSNESSVJKKXKWVNVKWWSSKVFLGTWZJ
# FKMVKKNSVFKVVKZKWWJJLMWKJKUVVMVKKZJKWZWWJKJKNEWV
