# Version 2.0
## language: en

Feature: Training programming
  Site:
    http://wargame.kr
  Category:
    vulnerability
  User:
    karlhanso82
  Goal:
    find the vulnerability in the system

 Background:
 Hacker's software:
   | <Software name> | <Version>    |
   | Windows OS      | 10           |
   | Chromium        | 74.0.3729.131|

 Machine information:
   Given the challenge URL
   """
   http://wargame.kr:8080/tmitter/
   """
   Then I open the url in chromium
   And I saw a twitter page with options sign in and sign up
   Then I try to create an account with sign up option
   And enter some arbitrary credential
   Then I try to login with that account
   And then appears a twitter like page
   And then I suppose that is related with with database
   Given this hint
   """
   create table tmitter_user(
    idx int auto_increment primary key,
    id char(32),
    ps char(32)
    );
   """
   And this other one
   """
   you need login with "admin"s id!
   """

 Scenario: Fail:entering-sql-injection
   Given the challenge url
   Then I saw the input fields of the sign in page
   Then I enter some valid sql injection code
   """
   or 1=1
   """
   Then I saw the page and nothing happens

  Scenario: Success:getting-the-flag
    Given the challenge
    Then I find a vulnerability related with the field of size of the table
    And then I enter this account at sign up option page
    """
    admin                           a
    """
    And I modify an admin input to maxlength 33
    """
    <input type="text" name="id" maxlength="32">
    """
    Then I enter the password with 7 characters since it's required
    Then I click the join button
    And this overwrites the admin account because the flaw
    And cuts the whitespaces and the a and replace it in the database
    Then the account ends overwrited
    And I log in at the sign in page
    Then I catch the flag
    And solve the challenge

