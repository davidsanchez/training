# Version 2.0
## language: en

Feature: Training programming
  Site:
    http://wargame.kr
  Category:
    vulnerability
    hacking
  User:
    karlhanso82
  Goal:
    find the vulnerability given a challenge

 Background:
 Hacker's software:
   | <Software name> | <Version>    |
   | Windows OS      | 10           |
   | Chromium        | 74.0.3729.131|

 Machine information:
   Given the challenge URL
   """
   http://wargame.kr:8080/md5_compare/
   """
   Then I open the url with Chromium
   And I see two input with the names
   """
   Value1
   Value2
   """
   And a button called chk
   Then I enter some values in these two inputs
   And recieve a message wrong
   Then I see  link to the source code of that page
   And I concluded that it must be a comparison vulnerability

 Scenario: Success:getting-the-flag
   Then I open the web browser with challenge url
   And I saw two input boxes
   Then I inspect relevant part of the code
   """
   if (!ctype_alpha($v1)) {$chk = false;}
        if (!is_numeric($v2) ) {$chk = false;}
        if (md5($v1) != md5($v2)) {$chk = false;}

        if ($chk){
            include("../lib.php");
            echo "Congratulations! FLAG is : ".auth_code("md5_compare");
        } else {
            echo "Wrong...";
        }
   """
   And I search for way to circumvent the md5 if
   Then I find that if I enter two especial values
   And the first value QNKCDZO
   Then the second value 240610708
   And the flaw is given the == operator
   Then it compares both hashes that begin with 0e
   And with these values we solve the challenge

