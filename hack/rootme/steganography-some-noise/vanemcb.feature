Feature: Solve Some noise
  From the Root Me site
  Category Steganography
  With my username vanem_cb

Background:
  Given I am running Windows 10 (64 bits)
  And I am using Google Chrome Version 70.0.3538.77 (Build oficial) (64 bits)

Scenario: Normal use case
  Given the challenge URL
  """
  https://www.root-me.org/en/Challenges/Steganography/Some-noise-40
  """
  Then I opened the URL with Google Chrome
  And I see the challenge statement
  """
  Some noise
  A little bit music ?
  """
  And I see the input field to submit the answer
  And I see a button
  """
  Start the challenge
  """
  When I click the button
  Then an audio file is downloaded
  """
  ch3.wav
  """
  Then I play the audio file
  And I listen something that I don't understand
  And I don't write nothing in the input field to submit the answer
  And I don't solve the challenge
  But I conclude that the audio pitch has been modified

Scenario: Failed Attempt number 1
  Given the audio file by clicking the button
  Then I open an audio edit software
  """
  Pro tools 10 HD
  """
  And I try to import the audio file
  But the software doesn't allow me to import it
  Then I think that is possible that the file's extension is wrong
  And I open the file's propierties
  And I see that the file's extension is rigth
  Then I can't import the audio file to edit
  And I don't write nothing in the input field to submit the answer
  And I don't solve the challenge
  But I conclude that there is something weird with the file's extension

Scenario: Successful solution
  Given the audio file by clicking the button
  Then I open a tool that can identify file formats
  """
  HexBrowserNET.exe downloaded of http://www.hexbrowser.com
  """
  And I import the audio file
  And I see the "File Type"
  """
  MP3 with ID3-tag
  """
  Then I change the file's extension to mp3
  """
  ch3.mp3
  """
  And I open an audio edit software
  """
  Pro tools 10 HD
  """
  And I import the audio file
  And I open the plug-in "Pitch Shift"
  Then I decrease the audio's pitch by 20 semitones
  When I apply this change the audio time is also proportionally modified
  But I can't understand the audio
  And I realize that is possible that the audio is in reverse
  Then I open the plug-in "Reverse"
  And I apply this change to the audio
  Then I listen the audio
  And I understand some numbers and letters
  """
  3b27641fc5h0
  """
  Then I write this in the input field to submit the answer
  And I solve the challenge
