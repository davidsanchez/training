/*
$ Checkstyle lekriss.java #linting
$ javac lekriss.java #Compilation
 */
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.util.Collections;

public class lekriss {

  public static void main(String[] args) {

    Scanner input;
    File file;
    ArrayList<ArrayList<String>> principal;

    try {
      file = new File("DATA.lst");
      input = new Scanner(file);
      principal = new ArrayList<ArrayList<String>>();
      int testCases = input.nextInt();

      while(testCases>0) {
        int numberOfNames = input.nextInt();
        ArrayList<String> temp = new ArrayList<String>();
        for(int i=0; i<numberOfNames; i++) {
          if(input.hasNext())
            temp.add(input.next());
        }
        Collections.sort(temp);
        principal.add(temp);
        testCases--;
      }

      for(ArrayList<String> sorting: principal) {
        int i = 0;
        int control =sorting.size();
        while(i<control-1) {
          if(sorting.get(i).equals(sorting.get(i+1))) {
            sorting.remove(i+1);
            control--;
          }
          else {
            i++;
          }
        }
      }
      for(ArrayList<String> sorted: principal){
        for(String chain: sorted)
            System.out.print(chain + " ");
        System.out.print("\n");
      }
    }

    catch (Exception e){
      e.printStackTrace();
    }
  }
}
/*
$ java lekriss
Andrew Danna Manuel Stenven Steven
Elias José Manuel
José Juan Maria
Alejandra Jose Manuel Martín Sofía
*/
