/*
$pmd.bat -d karlhanso82.java -R rulesets/java/quickstart.xml -f text #linting
may 16, 2019 9:35:03 AM net.sourceforge.pmd.PMD processFiles
WARNING:
This analysis could be faster, please consider using
Incremental  Analysis:
https://pmd.github.io/pmd-6.14.0/pmd_userdocs_incremental_analysis.html
$ javac -d . karlhanso82.java #Compilation
*/
import java.io.*;
import java.util.*;
import java.text.DecimalFormat;
import java.util.Properties;
import java.net.URL;

class Countupdiag {

 public static void main(String arg[]) throws Exception {
  URL path = ClassLoader.getSystemResource("DATA.lst");
  InputStream input = new FileInputStream(path.getFile());
  Properties prop = new Properties();
  prop.load(input);
  int a = Integer.parseInt(prop.getProperty("a"));
  int b = Integer.parseInt(prop.getProperty("b"));
  String s = "";
  for (int i = a; i <= b; i++) {
   if (s.length() == 0) {
    System.out.println(i);
    s += i;
   } else {
    s += i;
    String format = "%" + s.length() + "s\n";
    System.out.printf(format, i);
   }
  }
 }
}
/*
$ java karlhanso82
counting diag
$
*/

