//Km/h a m/s

import java.util.Scanner;

public class Reto6 {
    public static void main(String[] args) {

        Scanner digit            = new Scanner(System.in);
        int kilometrosHora       = 0;
        final double CONSTANTE   = 0.27;
        double metrosSegundo     = 0;

        System.out.println("");
        System.out.println("*******");
        System.out.println("*Reto6*");
        System.out.println("*******");
   
        System.out.println("");
        System.out.println("*************************");
        System.out.println("*Conversor de velocidad de  Km/h a m/s*");
        System.out.println("*************************");
        System.out.println("");

        System.out.print("Ingrese por favor la velocidad en Km/h: ");
        kilometrosHora = digit.nextInt();
        System.out.println("");

        metrosSegundo = kilometrosHora*CONSTANTE;
        System.out.println("La velocidad en m/s es: "+metrosSegundo);
    }    
}
