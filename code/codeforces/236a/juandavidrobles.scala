object JuanDavidRobles236A {

  def main(args: Array[String]): Unit = {

    import scala.io.StdIn
    import java.util

    var string: String = StdIn.readLine()
    var words: Array[Char] = new Array[Char](30)
    var count: Int = 0

    for (i <- 0 until string.length){
      if (!arrayContains(words, string.charAt(i))){
        words(count) = string.charAt(i)
        count += 1
      }
    }

    if (count%2 == 0){
      println("CHAT WITH HER!")
    } else {
      println("IGNORE HIM!")
    }

  }

  def arrayContains(myString: Array[Char], char: Char): Boolean ={
    var boolean: Boolean = false
    for (i <- 0 until myString.length){

      if (myString.charAt(i) == char){
        boolean = true
      }
    }

    boolean
  }
}
