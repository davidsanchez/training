object JuanDavidRobles {
  def main(args: Array[String]) {

    import scala.io.{StdIn}

    val initialPos = StdIn.readLine()
    val finalPos = StdIn.readLine()

    var initialLetter: Int = initialPos.charAt(0)
    var finalLetter: Int = finalPos.charAt(0)

    var initialNumber: Int = Integer.parseInt(initialPos.substring(1, 2))
    var finalNumber: Int = Integer.parseInt(finalPos.substring(1, 2))

    var lettersDif : Int = finalLetter - initialLetter
    var numberDif : Int = finalNumber - initialNumber

    var max : Int = 0

    if (Math.abs(lettersDif) > Math.abs(numberDif)){
      max = Math.abs(lettersDif)
      println(max)
    } else {
      max = Math.abs(numberDif)
      println(max)
    }

    var movement : String = ""
    var actualLetter : Int = initialLetter
    var actualNumber : Int = initialNumber

    var a : Int = 0

    for (a <- 0 until max){
      if (actualLetter < finalLetter) {
        movement = movement + "R"
        actualLetter += 1
      } else {
        if (actualLetter > finalLetter) {
          movement = movement + "L"
          actualLetter -= 1
        }
      }

      if (actualNumber < finalNumber) {
        movement = movement + "U"
        actualNumber += 1
      } else {
        if (actualNumber > finalNumber) {
          movement = movement + "D"
          actualNumber -= 1
        }
      }

      println(movement)
      movement = ""
    }
  }
}
