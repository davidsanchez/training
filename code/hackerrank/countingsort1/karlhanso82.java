/*
$pmd.bat -d karlhanso82.java -R rulesets/java/quickstart.xml -f text #linting
may 13, 2019 9:35:03 AM net.sourceforge.pmd.PMD processFiles
ADVERTENCIA:
This analysis could be faster, please consider using
Incremental  Analysis:
https://pmd.github.io/pmd-6.14.0/pmd_userdocs_incremental_analysis.html
$ javac -d . karlhanso82.java #Compilation
*/
import java.io.*;
import java.math.*;
import java.net.URL;
import java.util.*;

class Countingsorting {

 public static void main(String args[]) throws Exception {
  URL path   = ClassLoader.getSystemResource("DATA.lst");
  File file  = new File(path.getFile());
  Scanner sc = new Scanner(file);
  BufferedWriter bf = new BufferedWriter(new FileWriter("res.tx"));

  int n = sc.nextInt();
  sc.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

  String[] arrItems = sc.nextLine().split(" ");
  sc.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
  int[] freq = new int[100];

  for (int j = 0; j < n; j++) {
   int numb = Integer.parseInt(arrItems[j]);
   freq[numb] = freq[numb] + 1;
  }

  for (int i = 0; i < n; i++) {
   bf.write(String.valueOf(freq[i]));
   if (i != freq.length - 1) {
    bf.write(" ");
   }
  }
  bf.newLine();
  bf.close();
  sc.close();
 }
}
/*
$ java karlhanso82
Counting sorting 1
$
*/

