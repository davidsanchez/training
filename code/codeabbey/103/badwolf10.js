/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

/* eslint-disable filenames/match-regex */
/* eslint-disable no-bitwise */
/* eslint-disable fp/no-unused-expression*/

function getEggStates(eggar, eggst) {
  if (eggar.length > 0) {
    // eslint-disable-next-line no-bitwise
    return getEggStates(eggar.slice(1), (eggst * 2) | eggar[0]);
  }
  return eggst;
}

function getRuleWord(eggs, acc, dsize) {
  if (eggs.length !== 0) {
    const eggpow = 1 << (dsize - eggs[0]);
    return getRuleWord(eggs.slice(1), acc + eggpow, dsize);
  }
  return acc;
}

function testRule(ruleword, rulenum, eggstates) {
  const rules = ruleword.filter((x, i) => ((1 << i) & rulenum) === (1 << i));
  const ruleapply = rules.reduce((curr, acc) => curr ^ acc);
  if (eggstates === ruleapply) {
    return true;
  }
  return false;
}

const tco = require('tco');
const tryRules = tco((index, ruleword, eggstates) => {
  if (testRule(ruleword, index, eggstates)) {
    // TCO Optiminzation requires null return
    // eslint-disable-next-line fp/no-nil
    return [ null, index ];
  }
  return [ tryRules, [ index + 1, ruleword, eggstates ] ];
});

function printTouches(touches, index) {
  if (touches[touches.length - 1] === 1) {
    // eslint-disable-next-line no-console
    console.log(index);
  }
  if (touches.length === 0) {
    return 0;
  }
  return printTouches(touches.slice(0, touches.length - 1), index + 1);
}

function findTouches(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const dataLines = contents.split('\n');
  const eggstates = getEggStates(dataLines[0].split(' ').map(Number), 0);
  const ruleword = dataLines.slice(1).map((row) =>
    row.split(' ').slice(1).map(Number)).map((x) =>
    getRuleWord(x, 0, dataLines.length - 2));
  const touches = tryRules(1, ruleword, eggstates).toString(2)
    .split('').map(Number);
  printTouches(touches, 0);
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    findTouches(readerr, contents));
}

main();
/*
$ node badwolf10.js
0 2 3 6 8 12 16
*/
