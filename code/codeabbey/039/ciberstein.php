<?php
/*
PHP version 7.2.11.0

Linting with "SublimeLinter-PHP"

phpcs ciberstein.php

Compiling and linking using the "Command Windndsows prompt"

> C:\\..\php \\..\ciberstein.php
./output
*/
if(file_exists("DATA.lst")) {
  $file = fopen("./DATA.lst", "r");
  $ncases = fgets($file, 128);

  for($g = 0 ; $g < $ncases ; $g++) {
    $cant = explode(' ',fgets($file, 128));
    $X = $dnds = 0;

    for($i = 1 ; $i < 15 ; $i++)
      $X += $cant[$i];

    $op = $X / 14;

    for($i = 1 ; $i < 15 ; $i++)
      $dnds += pow($op - $cant[$i],2);

    $dnds = sqrt($dnds / 14);

    if($op / 25 < $dnds)
      print $cant[0].' ';
  }
    fclose($file);
}
else
  echo 'Error DATA.lst not found';
/*
./ciberstein.php
GOLD
*/
?>
