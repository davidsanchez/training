#include <iostream>
#include <map>

using namespace std;

map<char,int> map_init(void){
    map<char,int> kingdom;
    for(int i=65;i<91;i++){
        kingdom.insert(pair<char,int>((char)i,0));
    }
    return kingdom;
}

void cycle_det(int num){
    int n[num]={0};
    for(int i=0;i<num;i++){
        int numr;
        cin>>numr;
        map<char,int> kingdom=map_init();
        for(int j=0;j<numr;j++){
            char road[4];
            cin>>road;
            if((kingdom[road[0]]==1)&&(kingdom[road[2]]==1)){
                n[i]=1; break;
            }
            kingdom[road[0]]=1; kingdom[road[2]]=1;
        }
    }
    for(int i=0;i<num;i++){
        cout<<n[i]<<' ';
    }
}

int main(void){
    int num;
    cin>>num;
    cycle_det(num);
    return 0;
}
