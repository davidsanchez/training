program sumaPascal; {Nombre del programa}



var

Num1, Num2, res: Integer; {Declaro variables como enteras}



begin

{Inicializo variables para prevenir error de compilacion}

Num1:=0;

Num2:=0;

res:=0;



{Solicito numero 1}

writeLn('Introduzca el primer numero: ');

readLn(Num1);



{Solicito numero 2}

writeLn('Introduzca el segundo numero: ');

readLn(Num2);



{Calculo e imprimo}

res:=Num1+Num2;

writeLn(res);



end.
