'''
$mix credo --strict badwolf10.exs #linting
Checking 1 source file ...

Please report incorrect results: https://github.com/rrrene/credo/issues

Analysis took 0.2 seconds (0.02s to load, 0.2s running checks)
8 mods/funs, found no issues.

$mix compile badwolf10.exs #compilation
'''

defmodule Main do
@moduledoc """
Main Module
"""
  def getchunk(contents, chunk) do
    case chunk do
      0 -> Enum.drop(contents, -8)
      1 -> Enum.drop(Enum.drop(contents, -4), 4)
      2 -> Enum.drop(contents, 8)
    end
  end
  def gethillmap(hillmap) do
    strmap = String.split(hillmap, " ", trim: true)
    strmap |> Enum.map(&String.to_integer/1)
    |> Enum.with_index(0)
    |> Enum.map(fn {k,v} -> {v,k} end)
    |> Map.new
    |> expandmap(0,%{})
  end
  def expandmap(hillmap, i, xmap) do
    if hillmap[i] == nil do
      xmap
    else
      cval = hillmap[i] * 4
      base = 4 * i
      m = Map.merge xmap,
      %{base => cval, base + 1 => cval, base + 2 => cval, base + 3 => cval}
      expandmap(hillmap, i + 1, m)
    end
  end
  def getimpact(hillmap, shotvals ) do
    if Enum.empty? shotvals do
      nil
    else
      shotparam =  String.split(List.first(shotvals), " ")
      IO.puts findimpactspot(hillmap, shotparam, 1)
      getimpact(hillmap, List.delete_at(shotvals, 0))

    end
  end
  def findimpactspot(hillmap, shotval, p) do
    v0 = String.to_integer List.first(shotval)
    angle = String.to_integer List.last(shotval)
    if hillmap[p] > parabola v0, angle, p do
      if hillmap[p-1] > parabola v0, angle, p - 0.0001 do
        p - 1
      else
        p
      end

    else
      findimpactspot(hillmap, shotval, p + 1)
    end
  end
  def parabola(v0, angle, x) do
    a = :math.pi * angle / 180.0
    x * :math.tan(a) - 4.905 * x * x *
    (1 / (v0 * v0 * (:math.pow(:math.cos(a), 2))))
  end
  def testshots(datachunk) do
    hillmap = gethillmap(List.first(datachunk))
    shots = List.delete_at(datachunk, 0)
    #IO.inspect shots
    getimpact(hillmap, shots)
  end
end

{:ok, contents} = File.read("DATA.lst")
lines = String.split(contents, "\n", trim: true)

chunk1 = Main.getchunk(lines, 0)
chunk2 = Main.getchunk(lines, 1)
chunk3 = Main.getchunk(lines, 2)

Main.testshots(chunk1)
Main.testshots(chunk2)
Main.testshots(chunk3)

'''
$elixirc badwolf10.exc
88
56
75
72
59
99
83
136
88
'''
