%checkcode('vanemcb')

format long
clear
clc
fileID = fopen('DATA.lst');
cellInput = textscan(fileID,'%s');
cellHour = cell(length(cellInput),4);
cellHourFinal = cell(length(cellInput{1,1}),2);
vecMin = zeros(1,2);
vecHour = zeros(1,2);
degreeM = (360/12)/60;
degreeM2 = 360/60;

%Cycle to organize the cell arrays
for i=1:length(cellInput{1,1})
  cellHour{i,1} = cellInput{1,1}{i,1}(1,1:2);
  cellHour{i,2} = str2double(cellInput{1,1}{i,1}(1,end-1:end));
  if cellHour{i,1}(1,end) == ':';
    cellHour{i,1} = cellHour{i,1}(1,1:end-1);
  end
  cellHour{i,1} = str2double(cellHour{i,1});
  switch cellHour{i,1}
    case 13
      cellHour{i,1} = 1;
    case 14
      cellHour{i,1} = 2;
    case 15
      cellHour{i,1} = 3;
    case 16
      cellHour{i,1} = 4;
    case 17
      cellHour{i,1} = 5;
    case 18
      cellHour{i,1} = 6;
    case 19
      cellHour{i,1} = 7;
    case 20
      cellHour{i,1} = 8;
    case 21
      cellHour{i,1} = 9;
    case 22
      cellHour{i,1} = 10;
    case 23
      cellHour{i,1} = 11;
    case 0
      cellHour{i,1} = 12;
  end
end

%Cycle to compute the hands positions
for k=1:length(cellInput{1,1})

  %Position hour hand
  %Quadrant I
  if cellHour{k,1} == 12 && cellHour{k,2} == 0
    vecHour(1,1) = 10;
    vecHour(1,2) = 16;
  elseif cellHour{k,1} >= 1 && cellHour{k,1} < 3
    vecHour(1,1) = 10 + 6*sind(cellHour{k,2}*degreeM+30*cellHour{k,1});
    vecHour(1,2) = 10 + 6*cosd(cellHour{k,2}*degreeM+30*cellHour{k,1});
  elseif cellHour{k,1} == 12 && cellHour{k,2} ~= 0
    vecHour(1,1) = 10 + 6*sind(cellHour{k,2}*degreeM);
    vecHour(1,2) = 10 + 6*cosd(cellHour{k,2}*degreeM);
  end

  %Quadrant II
  if cellHour{k,1} == 3 && cellHour{k,2} == 0
    vecHour(1,1) = 16;
    vecHour(1,2) = 10;
  elseif cellHour{k,1} >= 3 && cellHour{k,1} < 6
    vecHour(1,1) = 10 + 6*cosd((cellHour{k,2}*degreeM+30*cellHour{k,1})-90);
    vecHour(1,2) = 10 - 6*sind((cellHour{k,2}*degreeM+30*cellHour{k,1})-90);
  end

  %Quadrant III
  if cellHour{k,1} == 6 && cellHour{k,2} == 0
    vecHour(1,1) = 10;
    vecHour(1,2) = 4;
  elseif cellHour{k,1} >= 6 && cellHour{k,1} < 9
    vecHour(1,1) = 10 - 6*sind((cellHour{k,2}*degreeM+30*cellHour{k,1})-180);
    vecHour(1,2) = 10 - 6*cosd((cellHour{k,2}*degreeM+30*cellHour{k,1})-180);
  end

  %Quadrant IV
  if cellHour{k,1} == 9 && cellHour{k,2} == 0
    vecHour(1,1) = 4;
    vecHour(1,2) = 10;
  elseif cellHour{k,1} >= 9 && cellHour{k,1} < 12
    vecHour(1,1) = 10 - 6*cosd((cellHour{k,2}*degreeM+30*cellHour{k,1})-270);
    vecHour(1,2) = 10 + 6*sind((cellHour{k,2}*degreeM+30*cellHour{k,1})-270);
  end

  %Position minute hand
  %Quadrant I
  if cellHour{k,2} >= 1 && cellHour{k,2} < 15
    vecMin(1,1) = 10 + 9*sind((cellHour{k,2}*degreeM2));
    vecMin(1,2) = 10 + 9*cosd((cellHour{k,2}*degreeM2));
  elseif cellHour{k,2} == 0
    vecMin(1,1) = 10;
    vecMin(1,2) = 19;
  end

  %Quadrant II
  if cellHour{k,2} > 15 && cellHour{k,2} < 30
    vecMin(1,1) = 10 + 9*sind(180-(cellHour{k,2}*degreeM2));
    vecMin(1,2) = 10 - 9*cosd(180-(cellHour{k,2}*degreeM2));
  elseif cellHour{k,2} == 15
    vecMin(1,1) = 19;
    vecMin(1,2) = 10;
  end

  %Quadrant III
  if cellHour{k,2} > 30 && cellHour{k,2} < 45
    vecMin(1,1) = 10 - 9*cosd((270 - cellHour{k,2}*degreeM2));
    vecMin(1,2) = 10 - 9*sind((270 - cellHour{k,2}*degreeM2));
  elseif cellHour{k,2} == 30
    vecMin(1,1) = 10;
    vecMin(1,2) = 1;
  end

  %Quadrant IV
  if cellHour{k,2} > 45 && cellHour{k,2} < 60
    vecMin(1,1) = 10 + 9*sind((cellHour{k,2}*degreeM2));
    vecMin(1,2) = 10 + 9*cosd((cellHour{k,2}*degreeM2));
  elseif cellHour{k,2} == 45
    vecMin(1,1) = 1;
    vecMin(1,2) = 10;
  end

  cellHour{k,3} = vecHour;
  cellHour{k,4} = vecMin;
  cellHourFinal{k,1} = vecHour;
  cellHourFinal{k,2} = vecMin;
end

vecPos = cell2mat(cellHourFinal);
disp(vecPos);

%vanemcb
%7.849792302728199 4.398517441016789 1.440491353343619 7.218847050625474
%4.436896872599276 7.752360439504528 18.950697058314461 9.059243830591118
%4.204445042265590 8.447085729384876 10.000000000000000 1.000000000000000
%14.662875768741825 6.224077653700975 18.559508646656383 7.218847050625474
%13.134991388295692 4.884159013875446 7.218847050625474 18.559508646656383
