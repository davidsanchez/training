#!/bin/bash

#
# Problem #12 Modulo and time difference
#
function date_to_seconds () {

    date_array=($1)
    d=${date_array[0]}
    h=${date_array[1]}
    m=${date_array[2]}
    s=${date_array[3]}

    days=$((d*24*60*60))
    hours=$((h*60*60))
    minutes=$((m*60))
    seconds=$((s+0))
    
    let "result=days+hours+minutes+seconds"

    echo "$result"
}

function seconds_to_date () {
    
    date=$1
    count=0
    iterator=0
    while [[ "$date" -gt 0 ]]; do
        if [[ "$count" -lt 2 ]]
        then
            remainder[$iterator]=$((date%60))
            let "iterator+=1"
            let "count+=1"
            let "date=date/60"
        else
            remainder[$iterator]=$((date%24))
            let "iterator+=1"
            let "count=0"
            let "date=date/24"
        fi
    done
    
    if [[ ${#remainder[@]} -eq 3 ]]
    then
        remainder[$iterator]=1
    fi
    
    count=0
    for (( idx=${#remainder[@]}-1 ; idx>=0 ; idx-- )) ; do
        remainder_reversed[$count]="${remainder[idx]}"
        let "count+=1"
    done
    
    d=${remainder_reversed[0]}
    h=${remainder_reversed[1]}
    m=${remainder_reversed[2]}
    s=${remainder_reversed[3]}
    printf '(%s) ' "$d $h $m $s"
}

while read -r line || [[ -n "$line" ]]; do
    len=$(echo -n "$line" | wc -c)
    count=0
    if [[ "$len" -gt 3 ]]
    then
        date_in_seconds=($line)
        count_for=0
        count_for1=0
        count_for2=0
        for item in "${date_in_seconds[@]}";
        do
            if [[ "$count_for" -lt 4 ]]
            then
                date_1[$count_for1]=$item
                let "count_for1+=1"
            else
                date_2[$count_for2]=$item
                let "count_for2+=1"
            fi
            let "count_for+=1"
        done

        d1=$(date_to_seconds "${date_1[*]}")
        d2=$(date_to_seconds "${date_2[*]}")
        let "total=d2-d1"
        
        seconds_to_date "$total"
    fi

done < "$1"
