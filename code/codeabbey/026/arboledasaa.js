/*
$ eslint jarboleda.js
$
*/

function gcd(alpha, beta) {
  return ((beta === 0) ? alpha : gcd(beta, alpha % beta));
}

function gcdWrapper(alpha, beta) {
  return ((alpha > beta) ? gcd(alpha, beta) : gcd(beta, alpha));
}

function lcm(alpha, beta) {
  return alpha * beta / gcdWrapper(alpha, beta);
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const inputFile = contents.split('\n');
  const solvedArray = inputFile
    .slice(1)
    .map((element) => {
      const inputElements = element.split(' ');
      return ('('.concat(parseInt(gcdWrapper(inputElements[0],
        inputElements[1]), 10),
      ' ',
      parseInt(lcm(inputElements[0], inputElements[1]), 10), ')')
      );
    })
    .join(' ');
  const output = process.stdout.write(`${ solvedArray }\n`);
  return output;
}

const fileReader = require('fs');

function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node jarboleda.js
output:
(30 164250) (3 13485) (80 28160) (2 2346) (22 7788) (1 45) (100 77500)
 (1 12920) (39 30537) (3 26163) (1 20844) (2 14) (1 4730) (79 211641)
 (1 10) (30 48180) (160 184800) (2 40)
*/
