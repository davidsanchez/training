# $ julia
#
#   _       _ _(_)_     |  A fresh approach to technical computing
#  (_)     | (_) (_)    |  Documentation: https://docs.julialang.org
#   _ _   _| |_  __ _   |  Type "?help" for help.
#  | | | | | | |/ _` |  |
#  | | |_| | | | (_| |  |  Version 0.6.4 (2018-07-09 19:09 UTC)
# _/ |\__'_|_|_|\__'_|  |  Official http://julialang.org/ release
#|__/                   |  x86_64-w64-mingw32
# $ using Lint
# $ length(lintfile("actiradob.jl"))
# 0

function readDic()
  dic = Array{String}(0)
  open("db-ip.txt") do f
    line = 1
    while !eof(f)
      text = readline(f)
      push!(dic,text)
      line += 1;
    end
  end
  println("Dictionary read")
  return dic
end

function binarySearch(dic)
  answer = ""
  open("DATA.lst") do f
    ntestCases = parse(Int32,readline(f))
    lenDic = length(dic)
    answer = ""
    for i = 1:ntestCases
      check = false
      case = parse(Int, readline(f), 36)
      rangLow = 1
      rangHigh = lenDic
      while !check
        index = trunc(Int,(rangLow+rangHigh)/2)
        low, high, country = split(dic[index])
        low = parse(Int, low, 36)
        high = low + parse(Int, high, 36)
        if case < low
          rangHigh = index
        elseif case > high
          rangLow = index
        else
          answer = string(answer," ",country)
          check = true
        end
      end
    end
  end
  answer = answer[2:length(answer)]
  println(answer)
  return answer
end

dic = readDic()
binarySearch(dic)

# $ include("actiradob.jl")
# Dictionary read
# GB PK FR AU ZA US AU CN BH AL PL UG US HK ...
# ... AU CH DE FR RU HR US BR US RU JP DE US FR
