--[[
$ luacheck skhorn.lua
Checking skhorn.lua                        OK
Total: 0 warnings / 0 errors in 1 file
--]]

-- Check if files exist
local function file_exist(file)
  local f = io.open(file, "rb")
  if f then f:close() end
  return f ~= nil
end

-- Get line by line of the file
local function lines_from(file)
  if not file_exist(file) then return {} end
  local lines = {}
  for line in io.lines(file) do
    lines[#lines + 1] = line
  end
  return lines
end

local file = 'DATA.lst'
local lines = lines_from(file)
-- luacheck: no unused args
for k,v in pairs(lines) do
  if string.len(v) ~= 2 then

    local line_array = {}
    local index = 1

    for item in string.gmatch(v, "%S+") do
      line_array[index] = tonumber(item)
      index = index + 1
    end
    -- uncomment this and the one in line 68
    -- to see the sorting in each iteration
    -- print(table.concat(line_array, " "))

    local swap = 0
    local pass = 1
    local flag = false
    while not flag do
      pass = pass + 1
      for i=1, #line_array do

        if i ~= #line_array then
          if line_array[i] > line_array[i+1] then
            local tmp = line_array[i]
            line_array[i] = line_array[i+1]
            line_array[i+1] = tmp
            swap = swap + 1
          end
        end
      end
      local count = 0
      for j=1, #line_array do
        if j ~= #line_array then
          if line_array[j] < line_array[j+1] then
            count = count + 1
          end
        end
      end
      if count == #line_array-1 then
        flag = true
      end
      -- print(table.concat(line_array, " "), "SWAP:", swap, "PASS", pass)
    end
    print(pass, swap)
  end
end

--[[
$ lua5.3 skhorn.lua
16 87
--]]
