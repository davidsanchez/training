/**
 * Author: Kemquiros
 * Purpose: Solve the 27th CodeAbbey problem.
 * Language:  C
**/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Unsigned int has maximum ten digits
#define UINT_MAX 10

struct vector{
 int size;
 int *array;
};

typedef int bool;
#define true 1
#define false 0


void readArray(struct vector v);
void bubbleSort(struct vector v, int *pass, int *swap);

int main(){
 //input variables
 struct vector v;
 int pass = 0, swap = 0;

 //Read the array size from stdin
 scanf("%d", &v.size);
 //Assign the memory for the array
 v.array = (int *)malloc(sizeof(int)*v.size);
 //Read the array from stdin
 readArray(v);
 //Bubble sort
 bubbleSort(v,&pass,&swap);
 //Print results
 printf("%d %d\n",pass,swap);
}

void readArray(struct vector v){
 //Save the max Unsigned int digits
 //Plus one character from space or end of line
 char *s = (char *)malloc(sizeof(char)*(UINT_MAX+1));
 for(int i = 0; i < v.size; i++){
  scanf("%s",s);
  //String to int casting
  v.array[i] = atoi(s);
 }
}

void bubbleSort(struct vector v, int *pass, int *swap){
 bool isFinish = false;
 bool isSwap;
 int aux;
 while(!isFinish){
  isSwap = false;
  *pass = *pass + 1;
  //Swap from 1 to n-1
  for(int i=0;i<v.size-1;i++){
   if(v.array[i]>v.array[i+1]){
    //Swapping
    aux = v.array[i];
    v.array[i] = v.array[i+1];
    v.array[i+1] = aux;
    isSwap = true;
    *swap = *swap + 1;
   }
  }
  //If there's no swap then finish the function
  isFinish = !isSwap;
 }
}
