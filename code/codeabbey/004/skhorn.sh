#!/bin/bash
#
# Problem #4 Minimum of Two
#
while read -r line || [[ -n "$line" ]]; do
    len=$(echo -n "$line" | wc -c)
    if [[ "$len" -gt 2 ]]
    then
        count=1
        data_array=($line)
        if [[ "${data_array[0]}" -lt "${data_array[1]}" ]]
        then
            output_array[$count]="${data_array[0]}"
        else
            output_array[$count]="${data_array[1]}"
        fi
        let "count+=1"
    fi
    printf '%s ' "${output_array[@]}"

done < "$1"
