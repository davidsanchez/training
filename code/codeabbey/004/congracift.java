/**
 * $ pmd -d congracift.java -f text -R rulesets/java/quickstart.xml -version 1.5
 * \-language java -debug
 * $
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class congracift {

    public static void main(String[] args) throws FileNotFoundException,
     IOException {
    String STRING = " ";
    String line = STRING;
    String[] numbers = new String[1];
    boolean flag = false;
    FileReader fileReader = new FileReader("DATA.lst");
    BufferedReader buffer = new BufferedReader(fileReader);//load file in memory
    line = buffer.readLine();
    while (line != null){ //read line on line
      if (flag) {
        if (line.contains(STRING)) {
          numbers = line.split(STRING);
          if (Integer.parseInt(numbers[0]) < Integer.parseInt(numbers[1])) {
            System.out.print(numbers[0] + STRING);
          } else {
            System.out.print(numbers[1] + STRING);
          }
         }
      }
         line = buffer.readLine();
         flag = true;
    }
    buffer.close();
  }
}

/**
 * $ javac congracift.java
 * $ java congracift
 * $ -2605178 -8641437 -1673153 -8560512 391090 -6006084 -446852 -7897953
 *   5695945 6727636 -6232151 320213 -8684553 -7369740 -4312186 3082635 -9672707
 *  -7403785 -1345861 -9665427 -8571476 -5498134 1971156 3278711 1395820 1187365
 *  -6111861 -4366784
 */
