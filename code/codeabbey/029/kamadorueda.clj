; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting kamadoatfluid.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

; namespace
(ns kamadoatfluid.clj
  (:gen-class)
)

; tokenize whitespace separated string into a convenient type and container
(defn sintoc [wsstr]
  (map #(Integer/parseInt %) (clojure.string/split wsstr #" "))
)

; struct for a pair of objects
(defstruct pair
  :element
  :index
)

; add a sequential :index to every element in list 'c'
(defn add-index [c s]
  (map #(struct pair %1 %2) c (range 1 s))
)

; define a comparison operator between 'pair' based on :element
(defn comp-op-el [p1 p2]
  (if (< (:element p1) (:element p2)) true false)
)

; sort list of 'pair' (:element, :index) 'c' based on :element
(defn sort-by-element [c]
  (sort (comp comp-op-el) c)
)

; remove ':element' from list of 'pair' 'c'
(defn ext-index-from-pair [c]
  (map #(:index %1) c)
)

; return original indexes from sorted array of list 'c'
(defn get-sorted-indexes [c s]
  (ext-index-from-pair (sort-by-element (add-index c s)))
)

; parse DATA.lst and solve
(defn process_file [file]
  (with-open [rdr (clojure.java.io/reader file)]
    (doseq [line (line-seq rdr)]
      (let [c (sintoc line) s (count c)]
        (println (get-sorted-indexes c s))
      )
    )
  )
)

; fire it all
(defn -main [& args]
  (process_file "DATA.lst")
)

; $lein run
;  (12 8 5 15 2 13 4 10 3 16 11 9 1 6 14 7)
