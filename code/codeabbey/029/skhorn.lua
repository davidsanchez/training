--[[
$ luacheck skhorn.lua 
Checking skhorn.lua                   OK
Total: 0 warnings / 0 errors in 1 file
--]]

-- Check if files exist
local function file_exist(file)
  local f = io.open(file, "rb")
  if f then f:close() end
  return f ~= nil
end

-- Get line by line of the file
local function lines_from(file)
  if not file_exist(file) then return {} end
  local lines = {}
  for line in io.lines(file) do
    lines[#lines + 1] = line
  end
  return lines
end

local file = 'DATA.lst'
local lines = lines_from(file)
local index_arr = {}
-- luacheck: no unused args
for k,v in pairs(lines) do
  if string.len(v) ~= 2 then

    local line_array = {}
    local index = 1

    for item in string.gmatch(v, "%S+") do
      line_array[index] = tonumber(item)
      index_arr[item] = index
      index = index + 1
    end
    local flag = false
    while not flag do
      for i=1, #line_array do
        if i ~= #line_array then
          if line_array[i] > line_array[i+1] then
            local tmp = line_array[i]
            line_array[i] = line_array[i+1]
            line_array[i+1] = tmp
          end
        end
      end
      local count = 0
      for j=1, #line_array do
        if j ~= #line_array then
          if line_array[j] < line_array[j+1] then
            count = count + 1
          end
        end
      end
      if count == #line_array-1 then
        flag = true
      end
    end
    local output = ""
    for l=1, #line_array do
      l = tostring(line_array[l])
      output = output .. index_arr[l] .. " "
    end
    print(output)
  end
end
--[[
$ lua5.3 skhorn.lua 
12 8 5 15 2 13 4 10 3 16 11 9 17 1 6 14 7
--]]
