let num=read_line();;
let inp= 
    Str.split (Str.regexp ")") num |> 
  List.map (fun x -> Str.split (Str.regexp "[(, ]+") x |>
            List.map int_of_string) in

for i=0 to List.length (List.nth inp 0) -1 do
    
    let a = ref 2  in
    let n= List.nth (List.nth inp 0) i in
    while !a< n do
        let b= ((float_of_int n**2.0)-.((2.0*.(float_of_int n))*.float_of_int !a))/.((2.0*.(float_of_int n))-.2.0*.float_of_int !a) in
        if (b-.(floor(b +. 0.5)))=0.0 then 
                begin
                print_int (int_of_float (float_of_int !a**2.0+.b**2.0));
                print_string " ";
                a:= List.nth (List.nth inp 0) i +1;
                end
                else     a := 1 + !a
    done;
done;
