-- $ ghc -o kedavamaru kedavamaru.hs
--   [1 of 1] Compiling Main ( kedavamaru.hs, kedavamaru.o )
--   Linking code ...
-- $ hlint kedavamaru.hs
--   No hints

module Main where

  import System.IO
  import Control.Monad

  workit :: [Int] -> IO()
  workit [] = return ()
  workit (x:xs) =
    do
      let c = round ((fromIntegral x - 32) * 5.0 / 9.0)
      print c
      workit xs

  processfile :: Handle -> IO ()
  processfile ifile =
    do
      iseof <- hIsEOF ifile
      Control.Monad.unless iseof $
        do
          line <- hGetLine ifile
          let vector_ints = map read $ words line :: [Int]
          let x = tail vector_ints
          workit x
          processfile ifile

  main =
    do
      ifile <- openFile "DATA.lst" ReadMode
      processfile ifile
      hClose ifile

-- $ ./kedavamaru
--   0
--   247
--   116
--   26
--   42
--   38
--   146
--   57
--   139
--   77
--   94
--   117
--   309
--   232
--   264
--   11
--   138
--   169
--   278
--   132
--   126
--   263
--   293
--   138
--   262
--   123
--   170
--   298
--   154
--   11
--   121
--   154
--   257
--   237
