/*
With JavaScript - Node.js
Linting:   eslint jenniferagve.js
--------------------------------------------------------------------
✖ 0 problem (0 error, 0 warnings)
*/

function reverse(eachInNor, indexCoun, eachInInv) {
/* Reorder the differnt cardnumber arrays to be process*/
  const revIn = [ eachInNor[indexCoun] ];
  const inConc = eachInInv.concat(revIn);
  if (indexCoun === 0) {
    return inConc;
  }
  return reverse(eachInNor, indexCoun - 1, inConc);
}
/* eslint array-element-newline: ["error", "consistent"]*/
function defPositions() {
/* Initial positions of the numbers to be manipulated*/
  const posNumbers = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10',
    '11',
    '12',
    '13',
    '14',
    '15',
    '16',
  ];
  return posNumbers;
}

function mulByTwo(element, eachIn) {
/* Takes each to position of the cardnumber and apply the formula for luhn
algorithm */
  if ((element % 2) === 0) {
    const mulCount = String(parseInt(eachIn[element - 1], 10) * 2);
    if (mulCount.length > 1) {
      return (parseInt(eachIn[element - 1], 10) * 2) - parseInt('9', 10);
    }
    return parseInt(eachIn[element - 1], 10) * 2;
  }
  return parseInt(eachIn[element - 1], 10);
}

function fixArray(wrongIn, eachInv, fixComp, fixVal) {
/* For those who had a missing number, it replace the number for 0 or the
correct value once it was calculated*/
  if (fixComp === true) {
    const firstPart = eachInv.slice(0, wrongIn);
    const replaceVal = [ fixVal ];
    const secondPart = eachInv.slice(wrongIn + 1, eachInv.length);
    const zeroReplace = firstPart.concat(replaceVal, secondPart);
    return zeroReplace;
  }
  const firstPart = eachInv.slice(0, wrongIn);
  const replaceVal = [ String(fixVal) ];
  const secondPart = eachInv.slice(wrongIn + 1, eachInv.length);
  const zeroReplace = firstPart.concat(replaceVal, secondPart);
  return zeroReplace;
}

function wrongTyp(counArr, eachInv, wrongIn) {
/* For those who had a missing number, this function calculate the missing
number base on the total sum and if it is multiple of 10*/
  const eachInInvF = fixArray(wrongIn, eachInv, true, '0');
  const arrayEx = counArr.map((element) => mulByTwo((parseInt(element, 10)),
    eachInInvF, counArr));
  const sumTotal = arrayEx.reduce((total, currentval) => total + currentval);
  const diff = ((parseInt((sumTotal / parseInt('10', 10)), 10) +
   1) * parseInt('10', 10)) - sumTotal;
  const divCount = parseInt(diff + parseInt('9', 10), 10);
  if ((((wrongIn + 1) % 2) === 0) && ((divCount % 2) > 0)) {
    const fixedAcc = fixArray(wrongIn, eachInv, false,
      (divCount - parseInt('9', 10)) / 2);
    const fixRev = [];
    const finalFix = reverse(fixedAcc, (fixedAcc.length - 1), fixRev);
    return (finalFix.join(''));
  } else if ((((wrongIn + 1) % 2) > 0)) {
    const fixedAcc = fixArray(wrongIn, eachInv, false, diff);
    const fixRev = [];
    const finalFix = reverse(fixedAcc, (fixedAcc.length - 1), fixRev);
    return (finalFix.join(''));
  }
  const fixedAcc = fixArray(wrongIn, eachInv, false, divCount / 2);
  const fixRev = [];
  const finalFix = reverse(fixedAcc, (fixedAcc.length - 1), fixRev);
  return (finalFix.join(''));
}

function swapTyp(eachInv, counArr, indexStart) {
/* For those who had its numbers swapped, this function alternate the numbers
until it found the correct sequence*/
  const changeIndex = eachInv.length - indexStart;
  const changNext = changeIndex + 1;
  const firstPart = eachInv.slice(0, changeIndex);
  const firstValue = eachInv[changNext];
  const secondValue = eachInv[changeIndex];
  const secondPart = eachInv.slice(changeIndex + 2, eachInv.length);
  const changeArray = firstPart.concat(firstValue, secondValue, secondPart);
  const arrayEx = counArr.map((element) => mulByTwo((parseInt(element, 10)),
    changeArray, counArr));
  const sumTotal = arrayEx.reduce((total, currentval) => total + currentval);
  const tenMod = sumTotal % parseInt('10', 10);
  if (tenMod === 0) {
    return changeArray;
  }
  return swapTyp(eachInv, counArr, indexStart + 1);
}

function correcCalc(oneCard) {
/* Defines if which type of error is in the card and calls each function
to solve the problem*/
  const dataInput = oneCard;
  const eachInNor = (dataInput.split('')).map((element) => element);
  const counArr = defPositions;
  const eachInInv = [];
  const eachInv = reverse(eachInNor, (eachInNor.length - 1), eachInInv);
  const wrongIn = eachInv.indexOf('?');

  if (wrongIn > parseInt('-1', 10)) {
    const finalFix = wrongTyp(counArr, eachInv, wrongIn);
    const output = process.stdout.write(`${ finalFix } `);
    return output;
  }
  const fixedAcc = swapTyp(eachInv, counArr, 2);
  const fixRev = [];
  const finalFix = reverse(fixedAcc, (fixedAcc.length - 1), fixRev);
  const unFinalFix = finalFix.join('');
  const output = process.stdout.write(`${ unFinalFix } `);
  return output;
}

function numbersProcess(erro, contents) {
/* where every cardnumber from the list is separated and passed to be
fixed*/
  if (erro) {
    return erro;
  }
  const inputFile = contents.split('\n');
  const inputNums = inputFile.slice(1);
  const correcVals = inputNums.map((element) => correcCalc(element));
  return correcVals;
}

const filesRe = require('fs');
function fileLoad() {
/* Load of the file to read all the data from the cards*/

  return filesRe.readFile('DATA.lst', 'utf8', (erro, contents) =>
    numbersProcess(erro, contents));
}
/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
node jenniferagve.js
input:4
?942682966937054
1217400151414995
2146133934?67114
2553514623364925
-------------------------------------------------------------------------
output:3942682966937054 1217040151414995 2146133934667114 2553514623369425
*/
