# $ruby-lint kergrau.rb

answer = ''
# open file code and itaration I take of
File.open('DATA.lst', 'r') do |file|
  while line = file.gets
    octal = /^0[0-7]+/.match(line)
    decimal = /^[1-9]\d*/.match(line)
    hexadecimal = /(0(x|X)\h+)|(\h+(h|H)?)/.match(line)
    binary = /(0(b|B)(0|1)*)|(1(0|1)+(b|B))/.match(line)

    if octal.to_s.strip == line.to_s.strip
      answer << 'oct '
    elsif decimal.to_s.strip == line.to_s.strip
      answer << 'dec '
    elsif binary.to_s.strip == line.to_s.strip
      answer << 'bin '
    elsif hexadecimal.to_s.strip == line.to_s.strip
      answer << 'hex '
    else
    end
  end
end
puts answer

#ruby kergrau.rb
=begin
oct dec dec bin hex dec dec hex hex oct bin hex bin hex hex oct oct dec dec
dec bin hex hex dec oct hex hex oct oct bin oct bin bin hex
=end
