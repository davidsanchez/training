/*
  $ php -l congracift.php
  $ No syntax errors detected in congracift.php
*/

<?php
 $fp=fopen("DATA.lst", "r+");
 stream_get_line($fp, 1024 * 1024, "\n");
 while ($line = stream_get_line($fp, 1024 * 1024, "\n")){
   list($numInitial, $increments, $iterations) = explode(' ', $line);
   $sum = ((($numInitial + $numInitial ) + $increments*($iterations - 1))/2);
   $sum = $sum * $iterations;
   echo $sum ." ";
 }
 fclose($fp);
?>

/*
  # sudo php -S 127.0.0.1:30 congracift.php
  $ curl http://127.0.0.1:30/congracift.php
  $ 91180 49039 29762 9860 3552 867 97119 3225 779 3654
*/
