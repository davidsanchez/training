﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallange
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            Int64 minus;
            String[,] val = new String[n, 3];
            String Line;
            String[] lspl;
            for (int i = 0; i < n; i++)
            {
                Line = Console.ReadLine();
                lspl = Line.Split(' ');
                for (int j = 0; j < 3; j++)
                {
                    val[i, j] = lspl[j];
                }
            }
            for (int k = 0; k < n; k++)
            {
                if (Int64.Parse(val[k, 0]) > Int64.Parse(val[k, 1]))
                {
                    minus = Int64.Parse(val[k, 1]);
                }
                else
                {
                    minus = Int64.Parse(val[k, 0]);
                }
                if (minus > Int64.Parse(val[k, 2]))
                {
                    minus = Int64.Parse(val[k, 2]);
                }
                Console.Write(minus + " ");

            }
            Console.ReadKey(true);
        }
    }
}
