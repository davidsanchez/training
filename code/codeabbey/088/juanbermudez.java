package RETO088;

public class CalculateFrequencys {
  public static void main(String[] args) {
    String[] notes= {"G#5","E2","G4","A#1","D1","C#1",
    "F#5","G2","C#3","E5","C#2","D5","C1","G#3","A#4",
    "D#3","G3","G#2","F2","B5","A#3","G#4"};
    for (String n : notes) {
      System.out.print(Math.round(Frequency.setFrequency(n))+" ");
    }
  }
}

class Frequency{
  public static double setFrequency(String note) {
    int numNote=0;
    int octaves=0;
    double frequency=0;
    String noteNext;
    String noteInit;
    switch (note.length()) {
    case 1:
      for (int i = 0; i < NOTES.length; i++) {
        if(note.equals(NOTES[i])) {
          numNote=i+1;
        }
      }
      octaves=0;
      break;
    case 2:
      noteNext=note.substring(1,2);
      noteInit=note.substring(0,1);
      if (noteNext.equals("#")) {
        for (int i = 0; i < NOTES.length; i++) {
          if(note.equals(NOTES[i])) {
            numNote=i+1;
          }
        }
        octaves=0;
        frequency=440*Math.exp(((octaves-4)+(numNote-10)/12.0)*Math.log(2));        
      }else {
        for (int i = 0; i < NOTES.length; i++) {
          if(noteInit.equals(NOTES[i])) {
            numNote=i+1;
          }
        }
        octaves=Integer.parseInt(noteNext);
      }
      break;
    case 3:
      noteInit=note.substring(0,2);
      noteNext=note.substring(2,3);
      for (int i = 0; i < NOTES.length; i++) {
        if(noteInit.equals(NOTES[i])) {
          numNote=i+1;
        }
      }
      octaves=Integer.parseInt(noteNext);
      break;
    default:
      break;
    }
    frequency=440*Math.exp(((octaves-4)+(numNote-10)/12.0)*Math.log(2));
    return frequency;
  }
  static final String[] NOTES= {"C","C#","D","D#","E","F","F#","G","G#","A","A#","B"};
}
