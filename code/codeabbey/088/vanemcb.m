%mcc -m vanemcb.m

%Cell array that contains the notes of a octave and the numbre of semitones
%until A4 of each note
cellOctave = {'C' 'C#' 'D' 'D#' 'E' 'F' 'F#' 'G' 'G#' 'A' 'A#' 'B';...
9 8 7 6 5 4 3 2 1 0 -1 -2}';

fileID = fopen('DATA.lst');
cellInput = textscan(fileID,'%s');
cellNotes = cell(length(cellInput{1,1}),2);
numNotes=length(cellInput{1,1});
vectFreq = zeros(numNotes,1);

%Cycle to organize the input notes and its octaves
for i = 1:numNotes
  cellNotes{i,1}=cellInput{1,1}{i,1}(1,1:end-1);
  cellNotes{i,2}=str2double(cellInput{1,1}{i,1}(1,end));
end

%Cycles to calculate the frequency of each note
for k=1:numNotes
  for y=1:12
    if strcmp(cellNotes{k,1},cellOctave{y,1}) == 1
      numSemiTones = cellOctave{y,2}+(12*(4-cellNotes{k,2}));
      vectFreq(k,1) = round(440*((2^(1/12))^(-numSemiTones)));
    end
  end
end
disp(vectFreq');

%vanemcb
%831 82 392 58 37 35 740 98 139 659 69 587 33 208 466 156 196 104 87 988
%233 415
