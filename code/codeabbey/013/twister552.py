data="""0 66 511 95646 331 2 508 16 230764 2673 267 23 4293406 4 1949 42733 205 2093256 9997543 47672608 892 297292253 4252912 59933 29727 12499863 15169 6 19 4740830 2094 161 8 28 254 136 9961236 12 78 84""".split()

for t in data:
    sum = 0
    position= len(t) #calculate max. digits
    t=int(t)
    while t > 0:
        sum = sum + (t % 10)*position
        t = t // 10
        position-=1
    print(sum, end=' ')
